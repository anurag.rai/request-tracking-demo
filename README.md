# Code changes required to have request tracking using CLS

* Using a cls module called cls-rtracer. Install with: `npm i cls-rtracer`
* Create a wrapper for the logger with cls module
* add option in cls module to read from header instead of creating new request-id (if required)
* add cls module in express middleware

``` 
const rTracer = require('cls-rtracer')
const loggerWrapper = {
    info: (data) => {
        logger.info( ` >>>> ${rTracer.id()} <<<< ${data}` )
    }
}
app.use(rTracer.expressMiddleware({
    useHeader: true,
    headerName: 'X-Request-Id',
}))
```

# To Test This Demo Project

Install dependencies
* `npm ci`

Start server
* `node server.js`

** There is a mock service with nested callbacks (BEFORE, NESTED, AFTER) with a delay on each callback to test if request-id is being passed in callbacks **

Send POST request with header as 'X-Request-Id' and json payload with field 'aaaa'
* `curl --location --request POST 'localhost:3000/data' --header 'X-Request-Id: a11-b11-1' --header 'Content-Type: application/json' --data-raw '{"aaaa": 1}'`

Response will look like
``` 
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":28521,"level":30,"msg":" >>>> a11-b11-1 <<<< In controller","time":"2020-10-22T18:14:06.962Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":28521,"level":30,"msg":" >>>> a11-b11-1 <<<< BEFORE. Data ---- 1","time":"2020-10-22T18:14:08.965Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":28521,"level":30,"msg":" >>>> a11-b11-1 <<<< NESTED. Data ---- 1","time":"2020-10-22T18:14:10.969Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":28521,"level":30,"msg":" >>>> a11-b11-1 <<<< AFTER. Data ---- 1","time":"2020-10-22T18:14:12.971Z","v":0}
```
______

To test asynchronous context, there is a delay time in `config.js` file which dictates how much time the callback in mock-services will be delayed. Change that to something like 3000 (3 seconds) and send many curl requests with different request-id and different data
E.g.
``` 
curl --location --request POST 'localhost:3000/data' --header 'X-Request-Id: a11-b11-1' --header 'Content-Type: application/json' --data-raw '{"aaaa": 1}'

curl --location --request POST 'localhost:3000/data' --header 'X-Request-Id: a55-d55-5' --header 'Content-Type: application/json' --data-raw '{"aaaa": 5}'

curl --location --request POST 'localhost:3000/data' --header 'X-Request-Id: a77-d77-7' --header 'Content-Type: application/json' --data-raw '{"aaaa": 7}'
```

Reponse for this should retain context (trace-id + data) for event API request:

``` 
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a11-b11-1 <<<< In controller","time":"2020-10-22T18:16:35.530Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a55-d55-5 <<<< In controller","time":"2020-10-22T18:16:35.547Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a77-d77-7 <<<< In controller","time":"2020-10-22T18:16:35.556Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a11-b11-1 <<<< BEFORE. Data ---- 1","time":"2020-10-22T18:16:37.535Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a55-d55-5 <<<< BEFORE. Data ---- 5","time":"2020-10-22T18:16:37.549Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a77-d77-7 <<<< BEFORE. Data ---- 7","time":"2020-10-22T18:16:37.558Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a11-b11-1 <<<< NESTED. Data ---- 1","time":"2020-10-22T18:16:39.537Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a55-d55-5 <<<< NESTED. Data ---- 5","time":"2020-10-22T18:16:39.553Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a77-d77-7 <<<< NESTED. Data ---- 7","time":"2020-10-22T18:16:39.559Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a11-b11-1 <<<< AFTER. Data ---- 1","time":"2020-10-22T18:16:41.541Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a55-d55-5 <<<< AFTER. Data ---- 5","time":"2020-10-22T18:16:41.554Z","v":0}
{"name":"traceLogger","hostname":"Anurags-MacBook-Pro.local","pid":30560,"level":30,"msg":" >>>> a77-d77-7 <<<< AFTER. Data ---- 7","time":"2020-10-22T18:16:41.561Z","v":0}
```
______

# Performance

## Benchmarks
- Performance benchmark for `cls-rtracer` [here](https://twitter.com/AndreyPechkurov/status/1268950294165143553?s=20)

## Issues
- Any CLS uses Node `async_hooks` which in itself degrades performance of service by a small margin. More discussion [here](https://github.com/nodejs/benchmarking/issues/181)
