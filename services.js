const delayTime = require('./config').delayTime
const serviceBoilerPlate = (logger) => {

    function nestedServiceLikeDBCall(data, cb) {
        setTimeout(() => {
            logger.info(`NESTED. Data ---- ${data}`)
            cb();
        }, delayTime)
    }

    return {
        serviceToDoSomething: (data) => {
            setTimeout(() => {
                logger.info(`BEFORE. Data ---- ${data}`)
                nestedServiceLikeDBCall(data, () => {
                    setTimeout(() => {
                        logger.info(`AFTER. Data ---- ${data}`)
                    }, delayTime);
                });
            }, delayTime)
        }
    }
}

module.exports = serviceBoilerPlate
