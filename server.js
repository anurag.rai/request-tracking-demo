const express = require('express')
const bodyParser = require('body-parser')
const babel = require('@smallcase/sc-babel');
const logger = babel.getLogger({
    loggerName: 'traceLogger',
    level: 'info',
    path: './sclogs_traceLogger',
    env: 'local'
});

// ---------- embedding cls in logger ---------- //
const rTracer = require('cls-rtracer')
const loggerWrapper = {
    info: (data) => {
        logger.info(` >>>> ${rTracer.id()} <<<< ${data}`)
    }
}
// --------------------------------------------- //

const services = require('./services')(loggerWrapper);
const controller = require('./controllers')(loggerWrapper, services)
const app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ extended: true }))
app.use(rTracer.expressMiddleware({
    useHeader: true,
    headerName: 'X-Request-Id',
}))
app.post('/data', controller.dataController);
app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:3000`);
})
